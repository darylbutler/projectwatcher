﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace FileHashWatcher
{
	public partial class MainForm : Form
	{
		List<Project> ProjectList;
		Timer UpdateTimer;


		public MainForm()
		{
			InitializeComponent();
			ProjectList = new List<Project>();
		}

		private void AddBtn_Click(object sender, EventArgs e)
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Multiselect = true;
			dlg.Filter = "All Files (*.*)|*.*";

			if (dlg.ShowDialog() == DialogResult.OK)
			{
				foreach (string path in dlg.FileNames)
					ProjectList.Add(new Project(path));
				
				UpdateGrid();
			}
		}

		private void UpdateGrid()
		{
			ProjectGrid.Rows.Clear();

			bool allMatching = true;

			foreach (Project prj in ProjectList)
			{
				DataGridViewRow row = new DataGridViewRow();

				bool matching = (prj.OriginalHash == prj.CurrentHash);

				allMatching &= matching;

				DataGridViewTextBoxCell cell = new DataGridViewTextBoxCell();
				cell.Value = prj.Path;
				cell.Style.BackColor = matching ? Color.LightGreen : Color.LightPink;
				row.Cells.Add(cell);

				DataGridViewTextBoxCell cell1 = new DataGridViewTextBoxCell();
				cell1.Value = prj.OriginalHash;
				cell1.Style.BackColor = matching ? Color.LightGreen : Color.LightPink;
				row.Cells.Add(cell1);

				DataGridViewTextBoxCell cell2 = new DataGridViewTextBoxCell();
				cell2.Value = prj.CurrentHash;
				cell2.Style.BackColor = matching ? Color.LightGreen : Color.LightPink;
				row.Cells.Add(cell2);

				DataGridViewTextBoxCell cell3 = new DataGridViewTextBoxCell();
				cell3.Value = matching;
				cell3.Style.BackColor = matching ? Color.LightGreen : Color.LightPink;
				row.Cells.Add(cell3);

				ProjectGrid.Rows.Add(row);
			}

			// Change notificion icon
			if (allMatching)
			{
				TrayIcon.Icon = FileHashWatcher.Properties.Resources.Good;
				TrayIcon.BalloonTipTitle = "All Files Unchanged";
				TrayIcon.BalloonTipText = "All Files Unchanged";
				TrayIcon.BalloonTipIcon = ToolTipIcon.Info;
			}
			else
			{
				TrayIcon.Icon = FileHashWatcher.Properties.Resources.Warning;
				TrayIcon.BalloonTipTitle = "Files Incorrect!";
				TrayIcon.BalloonTipText = "Some files were changed!";
				TrayIcon.BalloonTipIcon = ToolTipIcon.Error;
				TrayIcon.ShowBalloonTip(1000);
			}
		}

		private void PollingActiveCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			if (ProjectList.Count < 1)
				return;

			if (PollingActiveCheckBox.Checked)
			{
				// Check state just started
				PollingIntervalBox.Enabled = false;

				UpdateTimer = new Timer();
				UpdateTimer.Interval = (int)PollingIntervalBox.Value * 1000;
				UpdateTimer.Tick += UpdateTimer_Tick;
				UpdateTimer.Start();
			}
			else
			{
				// Timer turned off
				UpdateTimer.Stop();
				UpdateTimer.Dispose();
				PollingIntervalBox.Enabled = true;
			}
		}

		private void UpdateTimer_Tick(object sender, EventArgs e)
		{
			foreach (Project prj in ProjectList)
				prj.GetHashNow();
			UpdateGrid();
		}

		private void ReadNowBtn_Click(object sender, EventArgs e)
		{
			UpdateTimer_Tick(null, null);
		}
	}

	public class Project
	{
		// Path to the file
		private string path;
		public string Path { get { return path; } private set { path = value; } }

		// Hash of the file when this class is created
		private string originalHash;
		public string OriginalHash { get { return originalHash; } private set { originalHash = value; } }

		// Hash right now
		private string currentHash;
		public string CurrentHash { get { return currentHash; } private set { currentHash = value; } }

		// -- Constructor
		public Project(string _path)
		{
			Path = _path;
			GetHashNow();
			OriginalHash = CurrentHash;
		}

		// Update the CurrentHash value
		public void GetHashNow()
		{
			if (!File.Exists(Path))
				CurrentHash = "<File Not Found!>";
			else
			{
				using (SHA1 sha = SHA1.Create())
				{
					using (FileStream strm = File.OpenRead(Path))
					{
						byte[] result = sha.ComputeHash(strm);
						CurrentHash = BitConverter.ToString(result).Replace("-", "");
					}
				}
			}
		}
	}
}
