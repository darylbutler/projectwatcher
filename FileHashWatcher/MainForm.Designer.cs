﻿namespace FileHashWatcher
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.TrayIcon = new System.Windows.Forms.NotifyIcon(this.components);
			this.ProjectGrid = new System.Windows.Forms.DataGridView();
			this.Path = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.OriginalHash = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.CurrentChecksum = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Matching = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.AddBtn = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.PollingIntervalBox = new System.Windows.Forms.NumericUpDown();
			this.PollingActiveCheckBox = new System.Windows.Forms.CheckBox();
			this.ReadNowBtn = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.ProjectGrid)).BeginInit();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.PollingIntervalBox)).BeginInit();
			this.SuspendLayout();
			// 
			// TrayIcon
			// 
			this.TrayIcon.Text = "Project Watcher";
			this.TrayIcon.Visible = true;
			// 
			// ProjectGrid
			// 
			this.ProjectGrid.AllowUserToAddRows = false;
			this.ProjectGrid.AllowUserToDeleteRows = false;
			this.ProjectGrid.AllowUserToResizeRows = false;
			this.ProjectGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ProjectGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.ProjectGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.ProjectGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Path,
            this.OriginalHash,
            this.CurrentChecksum,
            this.Matching});
			this.ProjectGrid.Location = new System.Drawing.Point(12, 12);
			this.ProjectGrid.MultiSelect = false;
			this.ProjectGrid.Name = "ProjectGrid";
			this.ProjectGrid.ReadOnly = true;
			this.ProjectGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.ProjectGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
			this.ProjectGrid.ShowCellErrors = false;
			this.ProjectGrid.ShowCellToolTips = false;
			this.ProjectGrid.ShowEditingIcon = false;
			this.ProjectGrid.ShowRowErrors = false;
			this.ProjectGrid.Size = new System.Drawing.Size(957, 326);
			this.ProjectGrid.TabIndex = 0;
			// 
			// Path
			// 
			this.Path.HeaderText = "Path";
			this.Path.Name = "Path";
			this.Path.ReadOnly = true;
			// 
			// OriginalHash
			// 
			this.OriginalHash.HeaderText = "Original Checksum";
			this.OriginalHash.Name = "OriginalHash";
			this.OriginalHash.ReadOnly = true;
			// 
			// CurrentChecksum
			// 
			this.CurrentChecksum.HeaderText = "Current Checksum";
			this.CurrentChecksum.Name = "CurrentChecksum";
			this.CurrentChecksum.ReadOnly = true;
			// 
			// Matching
			// 
			this.Matching.FillWeight = 20F;
			this.Matching.HeaderText = "Matching";
			this.Matching.Name = "Matching";
			this.Matching.ReadOnly = true;
			// 
			// AddBtn
			// 
			this.AddBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.AddBtn.Location = new System.Drawing.Point(786, 344);
			this.AddBtn.Name = "AddBtn";
			this.AddBtn.Size = new System.Drawing.Size(183, 23);
			this.AddBtn.TabIndex = 1;
			this.AddBtn.Text = "Add New File...";
			this.AddBtn.UseVisualStyleBackColor = true;
			this.AddBtn.Click += new System.EventHandler(this.AddBtn_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.groupBox1.Controls.Add(this.PollingIntervalBox);
			this.groupBox1.Location = new System.Drawing.Point(12, 344);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(144, 50);
			this.groupBox1.TabIndex = 2;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Polling Interval (Seconds)";
			// 
			// PollingIntervalBox
			// 
			this.PollingIntervalBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.PollingIntervalBox.Location = new System.Drawing.Point(6, 19);
			this.PollingIntervalBox.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
			this.PollingIntervalBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.PollingIntervalBox.Name = "PollingIntervalBox";
			this.PollingIntervalBox.Size = new System.Drawing.Size(132, 20);
			this.PollingIntervalBox.TabIndex = 0;
			this.PollingIntervalBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// PollingActiveCheckBox
			// 
			this.PollingActiveCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.PollingActiveCheckBox.AutoSize = true;
			this.PollingActiveCheckBox.Location = new System.Drawing.Point(162, 364);
			this.PollingActiveCheckBox.Name = "PollingActiveCheckBox";
			this.PollingActiveCheckBox.Size = new System.Drawing.Size(90, 17);
			this.PollingActiveCheckBox.TabIndex = 3;
			this.PollingActiveCheckBox.Text = "Polling Active";
			this.PollingActiveCheckBox.UseVisualStyleBackColor = true;
			this.PollingActiveCheckBox.CheckedChanged += new System.EventHandler(this.PollingActiveCheckBox_CheckedChanged);
			// 
			// ReadNowBtn
			// 
			this.ReadNowBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.ReadNowBtn.Location = new System.Drawing.Point(619, 344);
			this.ReadNowBtn.Name = "ReadNowBtn";
			this.ReadNowBtn.Size = new System.Drawing.Size(161, 23);
			this.ReadNowBtn.TabIndex = 4;
			this.ReadNowBtn.Text = "Update Now";
			this.ReadNowBtn.UseVisualStyleBackColor = true;
			this.ReadNowBtn.Click += new System.EventHandler(this.ReadNowBtn_Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(981, 401);
			this.Controls.Add(this.ReadNowBtn);
			this.Controls.Add(this.PollingActiveCheckBox);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.AddBtn);
			this.Controls.Add(this.ProjectGrid);
			this.Name = "MainForm";
			this.Text = "Project Watcher";
			((System.ComponentModel.ISupportInitialize)(this.ProjectGrid)).EndInit();
			this.groupBox1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.PollingIntervalBox)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.NotifyIcon TrayIcon;
		private System.Windows.Forms.DataGridView ProjectGrid;
		private System.Windows.Forms.Button AddBtn;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.NumericUpDown PollingIntervalBox;
		private System.Windows.Forms.CheckBox PollingActiveCheckBox;
		private System.Windows.Forms.Button ReadNowBtn;
		private System.Windows.Forms.DataGridViewTextBoxColumn Path;
		private System.Windows.Forms.DataGridViewTextBoxColumn OriginalHash;
		private System.Windows.Forms.DataGridViewTextBoxColumn CurrentChecksum;
		private System.Windows.Forms.DataGridViewTextBoxColumn Matching;
	}
}

